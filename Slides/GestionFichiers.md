---
theme : "css/theme/synthwave.css"
revealOptions:
    transition: "zoom"
highlightTheme: "darkula"
separator: ^---$
verticalSeparator: ^--$
---

## Fichier et Serialisation

<small>Par [Vincent Lamouroux](https://gitlab.com/geekosaurusr3x)</small>
<small>Cour disponnible en ligne sur [https://geekosaurusr3x.gitlab.io/cours-ifa-tech](https://geekosaurusr3x.gitlab.io/cours-ifa-tech)</small>

---

## Actions sur les fichiers

--
### Lire un fichier texte par flux

- Utilisation de la classe `StreamReader`
- Permet de lire tous le fichier en une seule string
- Permet de lire le fichier ligne par ligne
  - Necessite une boucle
- Prend du temp
- Veroulle le fichier en access
- Doit etre fermé

--
```
using System.IO;

StreamReader sr = new StreamReader(path)
while (sr.Peek() >= 0);
{
  Console.WriteLine(sr.ReadLine());
}
sr.Close();
 
```
--
### Ecrire un fichier texte par flux

- Utilisation de la classe `StreamWriter`
- Permet d'ecore dans le fichier ligne par ligne
  - Necessite une boucle
- Prend du temp
- Veroulle le fichier en access
- Doit etre fermé

--
```
using System.IO;

List<string> lignes = new List<string>()
StreamWriter sw = new StreamWriter(path)
foreach (string s in lignes);
{
  sw.WriteLine(s);
}

sw.Close();
```
---
## Serialisation

- Permet de sauvegarder des données entre 2 utilisations du programe
- 3 formats utilisés
  - Binaire
  - JSON
  - XML

--
### Binaire

- Serialise l'ensemble des attributs de la classe
- Travaille sur un stream
- Pas héritable
--

```c#
[Serializable]  
public class MyObject {  
  public int n1 = 0;  
  public int n2 = 0;  
  public String str = null;  
}
```

--

```c#
using System.Runtime.Serialization
using System.Runtime.Serialization.Formatters.Binary;

MyObject obj = new MyObject();
IFormatter formatter = new BinaryFormatter();  
Stream stream = new FileStream("MyFile.bin", FileMode.Create, FileAccess.Write, FileShare.None);  
formatter.Serialize(stream, obj);  
stream.Close();  
```

--

```c#
IFormatter formatter = new BinaryFormatter();  
Stream stream = new FileStream("MyFile.bin", FileMode.Create, FileAccess.Write, FileShare.None);  
MyObject obj = (MyObject) formatter.Deserialize(stream);  
stream.Close();  
```
--
### Json

- Serialise l'ensemble des attributs public de la classe
- Travaille sur un stream
- Serialise en string
- Natif pour Si netcore > 3.0
- Installer Nuget System.Text.Json sinon
--

```c#
public class MyObject {  
  public int n1 = 0;  
  public int n2 = 0;  
  public String str = null;  
}
```

--

```c#
using System.Text.Json;
using System.Text.Json.Serialization;

MyObject obj = new Myobject();
string jsonString;
jsonString = JsonSerializer.Serialize(obj);
StreamWriter sw = new StreamWriter(path)
sw.WriteLine(jsonString);
sw.Close();  
```

--

```c#
string jsonString;

StreamReader sr = new StreamReader(path)
jsonString = sr.ReadLine();
sr.Close();  

MyObject obj;
obj = JsonSerializer.Deserialize<MyObject>(jsonString);
```

--
### XML

- Serialise l'ensemble des attributs public de la classe
- Travaille sur un stream
- Serialise en string
--

```c#
public class MyObject {  
  public int n1 = 0;  
  public int n2 = 0;  
  public String str = null;  
}
```

--

```c#
MyObject obj = new Myobject();
XmlSerializer x = new XmlSerializer(typeof(MyObject));
StreamWriter sw = new StreamWriter(path)
x.Serialize(sw, obj);
sw.Close();  
```

--

```c#
string jsonString;

XmlSerializer x = new XmlSerializer(typeof(MyObject));
StreamReader sr = new StreamReader(path);
MyObject obj = (MyObject) x.Deserialize(sr);
sr.Close();  

```
---
## Exos
--
### Exo 1
utilisation de l'ihm
Affichage d'une liste de voiture
Ajout / suppresion voiture dans liste
Save/Load liste dans fichier
--
### Exo 2
utilisation ihm
gestion de deck magick
cartes:
  - terrain
      nom
      description
  - creature
      cout
      nom
      description
      atk
      def
  - sort
      cout
      nom
      description
      texte d'efet

charger sauver deck en json
charger sauver liste carte complete


---
## Sources

- [File classe](https://docs.microsoft.com/fr-fr/dotnet/api/system.io.file?view=netframework-4.8)
- [How to write in file](https://docs.microsoft.com/fr-fr/dotnet/standard/io/how-to-write-text-to-a-file)
- [Serialisation](https://docs.microsoft.com/fr-fr/dotnet/standard/serialization/)