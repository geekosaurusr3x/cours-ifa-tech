---
theme : "css/theme/synthwave.css"
revealOptions:
    transition: "zoom"
highlightTheme: "darkula"
separator: ^---$
verticalSeparator: ^--$
---
# Windows Presentation Foundation

<small>Par [Vincent Lamouroux](https://gitlab.com/geekosaurusr3x)</small>
<small>Cour disponnible en ligne sur [https://geekosaurusr3x.gitlab.io/cours-ifa-tech](https://geekosaurusr3x.gitlab.io/cours-ifa-tech)</small>

---

### Presentation

--

- UI Framework pour applications natives
- Utilise de l' XAML pour definir l'interface
- Utilise du C# pour l'intereter

--

Presentation en live

- main window
- lbl
- button
- event
---
### DialiogBox
--
#### Message Box

```c#
MessageBox.show(contenu,titre,MessageBoxButton);
```
--
#### Open File Dialog Box

```c#
using Microsoft.Win32;
OpenFileDialog openFileDialog = new OpenFileDialog();
if(openFileDialog.ShowDialog() == true)
{
    string path = openFileDialog.FileName;
}
```
--
#### File dialog box option

```c#
OpenFileDialog.Filter= "Text files(*.txt)|*.txt|All files(*.*)|*.*";

```
```c#
OpenFileDialog.InitialDirectory= "Path";

```

--
#### MultiFiles

```c#
openFileDialog.Multiselect = true;
if(openFileDialog.ShowDialog() == true)
{
    foreach(string filename in openFileDialog.FileNames){
        //todo faire quelque chose
		filename;
    }
}
```
--
#### Save File Dialog Box

```c#
using Microsoft.Win32;
SaveFileDialog saveFileDialog = new SaveFileDialog();
if(saveFileDialog.ShowDialog() == true)
{
    string path = saveFileDialog.FileName;
}
```
--
#### Ouverture d'une nouvelle fenetre

```c#
public partial class DialogBox : Window
{
    public DialogBox()
    {
        InitializeComponent();
    }
}

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
        DialogBox box = new DialogBox();
        box.show();
    }
}
```
--
#### Custom DialogBox

--
```c#
	<TextBox Name="txtAnswer" Grid.Column="1" Grid.Row="1" MinWidth="250">Answer</TextBox>

	<WrapPanel HorizontalAlignment="Right" Margin="0,15,0,0">
		<Button IsDefault="True" Name="btnDialogOk" Click="btnDialogOk_Click" MinWidth="60" Margin="0,0,10,0">_Ok</Button>
		<Button IsCancel="True" MinWidth="60">_Cancel</Button>
	</WrapPanel>
```
```c#
public partial class InputDialogSample : Window
	{
		public InputDialogSample()
		{
			InitializeComponent();
		}

		private void btnDialogOk_Click(object sender, RoutedEventArgs e)
		{
			this.DialogResult = true;
		}

		public string Answer
		{
			get { return txtAnswer.Text; }
		}
	}
```
--

```c#
		private void OtherBtn_Click(object sender, RoutedEventArgs e)
		{
			InputDialogSample fenetre = new InputDialogSample();
			if(fenetre.ShowDialog() == true){
			string toto = fenetre.Answer;
			}
		}
```
---

### Lier une liste de données à une liste gui

--
#### Simple
--
```
<Window x:Class="ChangeNotificationSample"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        Title="ChangeNotificationSample" Height="135" Width="300">
		<ListBox Name="lbUsers" DisplayMemberPath="Name"></ListBox>
</Window>
```
```
	public partial class ChangeNotificationSample : Window
	{
		private List<User> users = new List<User>();

		public ChangeNotificationSample()
		{
			InitializeComponent();

			lbUsers.ItemsSource = users;
		}

        public void Updat()
        {
            users.add(new Users("test"));
            lbUser.Items.Refresh();
        }
    }
```
```
	public class User :
	{
		private string name;
		public string Name {
			get { return this.name; }
			set
			{
				if(this.name != value)
				{
					this.name = value;
				}
			}
		}
	}
```
--
#### Auto Sync
--
```
<Window x:Class="ChangeNotificationSample"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        Title="ChangeNotificationSample" Height="135" Width="300">
		<ListBox Name="lbUsers" DisplayMemberPath="Name"></ListBox>
</Window>
```
```
	public partial class ChangeNotificationSample : Window
	{
		private ObservableCollection<User> users = new ObservableCollection<User>();

		public ChangeNotificationSample()
		{
			InitializeComponent();

			lbUsers.ItemsSource = users;
		}
    }
```
--
```
	public class User : INotifyPropertyChanged
	{
		private string name;
		public string Name {
			get { return this.name; }
			set
			{
				if(this.name != value)
				{
					this.name = value;
					this.NotifyPropertyChanged("Name");
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public void NotifyPropertyChanged(string propName)
		{
			if(this.PropertyChanged != null)
				this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
		}
	}
```
--

### Mise en forme

Utilisation d'une ListView

```c#
<ListView Margin="10" Name="lvUsers">
	<ListView.ItemTemplate>
		<DataTemplate>
			<WrapPanel>
				<TextBlock Text="Name: " />
				<TextBlock Text="{Binding Name}" FontWeight="Bold" />
			</WrapPanel>
		</DataTemplate>
	</ListView.ItemTemplate>
</ListView>
```
--
### Datagrid (exel)

```c#
<DataGrid Name="DgUsers" AutoGenerateColumns="False">
	<DataGrid.Columns>
		<DataGridTextColumn Header="Name" Binding="{Binding Name}" />
		<DataGridTemplateColumn Header="Birthday">
			<DataGridTemplateColumn.CellTemplate>
				<DataTemplate>
					<WrapPanel>
						<TextBlock Text="Date: " />
						<TextBlock Text="{Binding Name}" FontWeight="Bold" />
					</WrapPanel>
				</DataTemplate>
			</DataGridTemplateColumn.CellTemplate>
		</DataGridTemplateColumn>
	</DataGrid.Columns>
```
--
### Menu

```
<Window.CommandBindings>
    <CommandBinding Command="ApplicationCommands.New." CanExecute="NewCommand_CanExecute" Executed="NewCommand_Executed" />
</Window.CommandBindings>
<DockPanel>
    <Menu DockPanel.Dock="Top">
        <MenuItem Header="File">
            <MenuItem Header="New" Command="ApplicationCommands.New" />
            <MenuItem Header="Open" />
            <MenuItem Header="Save" />
            <Separator />
            <MenuItem Header="_Exit" />
        </MenuItem>
    </Menu>
    <TextBox AcceptsReturn="True" />
</DockPanel>
```

--

```
	private void NewCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
	{
		e.CanExecute = true;
	}

	private void NewCommand_Executed(object sender, ExecutedRoutedEventArgs e)
	{
		txtEditor.Text = "";
	}
```

--
### Custom Command

- Deffinition d'une CommandRoutedUI

```c#
namespace ApplicationNameSpace.Commands{
    public class MyApplicationCommands
    {
        public static RoutedUICommand MyCustomCommand 
                               = new RoutedUICommand("My custom command", 
                                                     "MyCustomCommand", 
                                                     typeof(MyApplicationCommands));
    }
}²
```

```
<UserControl x:Class="..."
             ...
             xmlns:commands="clr-namespace:ApplicationNameSpace.Commands">
<Window.CommandBindings>
    <CommandBinding Command="{x:Static commands:MyApplicationCommands.MyCustomCommand}" CanExecute="NewCommand_CanExecute" Executed="NewCommand_Executed" />
</Window.CommandBindings>
```
--

#### Injection de sous parties

Ajouter un nouvelle element -> WPF -> Page
```
<Grid>
	<Frame Source="AutrePage.xaml">
</Grid>
```
--
#### Tabulation

```
<Grid>
	<TabControl Margin="0,26,10,10">
		<TabItem Header="Page1">
			Content .....
		</TabItem>
		<TabItem Header="Page2">
			Content ....
		</TabItem>
	</TabControl>
</Grid>
```
---
### Threading

--
#### Dispatcher

Permet de lancer une action tous les X temps

```
DispatcherTimer timer = new DispatcherTimer(); 
timer.Interval : le temps d'invocation
timer.Tick : la methode
```
```
void Methode(object sender, EventArgs e)
{
	//TODO : do something
}
```
--

```c#
	public MainWindow()
	{
		InitializeComponent();
		DispatcherTimer timer = new DispatcherTimer();
		timer.Interval = TimeSpan.FromSeconds(1);
		timer.Tick += timer_Tick;
		timer.Start();
	}

	void timer_Tick(object sender, EventArgs e)
	{
		lblTime.Content = DateTime.Now.ToLongTimeString();
	}
```
--
#### BackgroundWorker

Interdiction d'update la GUI par un thread autre que le principal
Utilisation d'une classe special BackgroundWorker

--
```
	public MainWindow()
	{
		BackgroundWorker worker = new BackgroundWorker();
		worker.WorkerReportsProgress = true; //permet de dire si on appel ProgressChanged
		worker.WorkerSupportsCancellation = true //permet de l'arreter
		worker.DoWork += worker_DoWork; //Methode pour faire le travails (run du thread)
		worker.ProgressChanged += worker_ProgressChanged; //Methode pour metre à jour la gui
		worker.RunWorkerCompleted += worker_RunWorkerCompleted; //methode appelé à la fin
		worker.RunWorkerAsync(arguement); //Thread.Start
		worker.CancelAsync(); // arreter le worker
	}
```

--
```
	private void worker_DoWork(object sender, DoWorkEventArgs e)
	{
		int max = (int)e.Argument; //permet de recuperer l'argument
		(sender as BackgroundWorker).ReportProgress(int); // permet de donner le status (% d'avancement)
		(sender as BackgroundWorker).ReportProgress(int,int); // permet de donner le status (% d'avancement et le UserState)
		e.Result = result; //return le resultat a la fin
	}
```

--
```
	private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
	{
		e.ProgressPercentage; // pour recupere le pourcentage
		if(e.UserState != null) //pour recupere un second champ passé un para
				lbResults.Items.Add(e.UserState);

		//Metre a jour lui gui pour la progression
	}
```

--
```
	private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
	{
		//Metre a jour lui gui pour afficher la fin de l'execution du thread
	}
```
---

Sources :

https://www.wpf-tutorial.com/
---
