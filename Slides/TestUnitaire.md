---
theme : "css/theme/synthwave.css"
revealOptions:
    transition: "zoom"
highlightTheme: "darkula"
separator: ^---$
verticalSeparator: ^--$
---

## Test Unitaire CSharp

<small>Par [Vincent Lamouroux](https://gitlab.com/geekosaurusr3x)</small>
<small>Cour disponnible en ligne sur [https://geekosaurusr3x.gitlab.io/cours-ifa-tech](https://geekosaurusr3x.gitlab.io/cours-ifa-tech)</small>

---

Test automatique de vos classes
Metthode du TDD (Test Driven Development)

Integré directement a L'IDE

---

### Mise en place

click droit sur la solution :
Ajouter nouveau projet -> Projet de test unitaire
Dans le nouveau projet -> ajouter une reference -> projet -> le projet a tester
--
#### Classe de test

- Using : Microsoft.VisualStudio.TestTools.UnitTesting;
- Classe attribut : [TestClass]
- Methode attribut : [TestMethod]
  - retroune void
  - ne prend pas de parametres
   
--

```C#
using Microsoft.VisualStudio.TestTools.UnitTesting;

[TestClass]
public class ClassedeTests
{
    [TestMethod]
    public void MethodeDeTest()
    {
        // test à faire
    }
}
```

--

#### Verification de votre resultat 

Utilisation de la classe static Assert :

- .AreEqual
- .IsFalse
- .IsTrue
- Etc

--

```C#
[TestMethod]
public void MethodeDeTest()
{
    int valeur = 10;
    int resultat = Math.Factorielle(valeur);
    Assert.AreEqual(1, resultat, "La valeur doit être égale à 1");
}
```

--
#### Initialisation et cleanup

- [TestInitialize]
  - Lancé avant chaque methode de test
  - Permet de specificer des données (creer des fichiers specifique)

- [TestCleanup]
  - Lancé apres chaque methode de test
  - Permet de netoyer les données de test

---
## Bonnes pratiques

- Faire des tests !!!!
- Toujours nomer la classe de test avec le nom de classe a tester + Test
  - 'MaClasseTest'
- Toujours nomer les methodes par ce qu'elles font :
  - 'TestFactorielle_Valeur3_Retourne6'
- Ne tester que les methodes publiques
- Bien choisir son jeux de donnée :
  - 1 test avec des données valides
  - 5 tests avec des données Exceptionnels (Cas limites)
- Avoir la meilleur couverture de code
  - 100 % impossible (code privé)