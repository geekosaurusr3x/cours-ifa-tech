---
theme : "css/theme/synthwave.css"
revealOptions:
    transition: "zoom"
highlightTheme: "darkula"
separator: ^---$
verticalSeparator: ^--$
---

### Singleton

Comment faire pour n'avoir qu'une seule instance d'un object ?

- S'assurer d'une seule instance de log
- N'avoir qu'une seule instance d'un joueur

--

- Un parametre `Prive Static Instance`
- Un constructeurs `Prive`
- Une méthode statique pur obtenir l'instance

```
  Personnage = Classe
    Private Static Instance : Personnage
    Private Fonction Personnage() : Null
    FinFonction
    Public Static Fonction GetInstance() : Personnage
      Si Instance == null alors
        Instance = new Personnage()
      Finsi
      Retourner Instance
    FinFonction
    FinClasse
```

--
### Factory

Sert à masquer la creation d'un object

--
```
Exo 1
Fabrique de chien / chat / oiseaux
```

--
### Prototype

![prototype](Img/prototype.gif)
Create Object with cloning it during execution

```
exo 1
Les couleurs sont définies par r g b
Le constructeur va metre 10 seconde à s'executer
(System.Threading.Thread.Sleep(1000);)
((color) this.MemberwiseClone())
Vous allez demander a l'utilisateur de creer 10 couleurs et à a chaque fois vous allez proposer le clonage ou la copie;
```


```
exo 2
Vous allez écrire un programme qui va créer 10 k instance d'un object contenu dans un fichier json.
Pour vous assurer que les instances sont différentes vous devez changer l'ID.
Vous devez afficher la liste des ID à la fin
Vous devez le faire le plus rapidement possible.
La classe est une classe livre :
- IDInstance
- Titre
- Auteur
- Resume

```
--
### Injection de dependences

- Fait d'injecter une classe dans une autre pour éviter les liaisons fortes.
- Plus de control sur les dépendances
- Eviter le nouveau dans une classe

--
```
  ConnecteurBaseDonnee = Classe
    var log : Loger
    Public Fonction ConnecteurBaseDonnee(var logger : Loger)
      log = loger
    FinFonction
  FinClasse
```

--
### Adapter

![AdapterGraph](/Img/adapter.gif)

Permet de convertir l'api d'une classe en une autre que le programe principal attend

--
```
Exo1
/// Soit les classes
///  Chien 
///     - aboyer
///  Chat
///     - mioler
///  Faire un for each de Ianimals 
///     item.produireSons()
```
--
```
Exo2
/// Soit une classe Banque de film legacy
///     string getRealisateur(nom)
///     string dateSortie(nom)
///     int    nombreEntree()
/// Adapter en Film
///     - Realisateur
///     - DateSortie
///     - NombreEntree
/// 
/// Stoquer une liste de Film et demander a l'utilisateur de quel film il veux voir les infos
```

--

### Décorateur

- Ajout de fonctionnalités dynamiques à l'execution
- Permet de limité l'heritage

--

```
  Voiture = Classe
    Private var prix : reel
    Public Fonction SetPrix(var p : reel) : null
      prix = p
    FinFonction
    Public Fonction GetPrix() : reel
      return prix
    FinFonction
  Fin Classe
```

```
  Option = Classe extend Voiture
    Prive var voiture : Voiture
    Prive var prixOption : reel
    Public Fonction Option(var voit : Voiture, var prix : reel) : Null
      voiture = voit
      prixOption = prix
    FinFonction
    Public Fonction GetPrix() : reel
      return voiture.prix + prixOption;
    FinFonction
  FinClasse
```
--
```
  VoitureAvecClim = Classe extend Option
    Public Fonction VoitureAvecClim(var voit : Voiture)
      Base(voit,10)
    FinFonction
```
```
  VoitureAvecToiOuvrant = Classe extend Option
    Public Fonction VoitureAvecToiOuvrant(var voit : Voiture)
      Base(voit,1000)
    FinFonction
```
--
```
var voit : Voiture
Debut
  voit = New Voiture()
  voit. prix = 5000
  voit = New VoitureAvecClim(voit)
  voit = New VoitureAvecToiOuvrant(voit)

  Afficher(voit) // 6010
```
--
### Composite

trop rare
- Representation d'une structure données de type arbre
- Permet de se passer de concepts comme les pointeurs

--
```
  Graphic = Interface
    Public Fonction Print() : Null
  FinInterface
```

```
  Ellipse = Classe implemente Graphic
    Public Fonction Print() : Null
      Afficher(Ellipse)
    FinFonction
  FinClasse
```
--
```
  CompositeGraphic = Classe Implemente Graphic
    Private Liste : Tableau [0 a 9] de type Graphic
    Private index : entier

    Public Fonction Print() : Null
      Pour i allant de 0 a index Faire
        Liste[i].Print()
      FinPour
    FinFonction

    Public Fonction Add(ref graph : Graphic) : Null
      Liste[index] = graph
      index = index +1
    FinFonction

    Public Fonction Remove() : Null
      index = index - 1
    FinFonction
  FinClasse
```
--
```
  var ellipse1 : Ellipse
  var ellipse2 : Ellipse
  var ellipse3 : Ellipse
  var ellipse4 : Ellipse
  
  Var graphic : CompositeGraphique
  Var graphic1 : CompositeGraphique
  Var graphic2 : CompositeGraphique
```
--
```
  Debut
    ellipse1 = New Ellipse()
    ellipse2 = New Ellipse()
    ellipse3 = New Ellipse()
    ellipse4 = New Ellipse()
    graphic = New CompositeGraphique()
    graphic1 = New CompositeGraphique()
    graphic2 = New CompositeGraphique()

    graphic1.Add(ellipse1)
    graphic1.Add(ellipse2)
    graphic2.Add(ellipse3)
    graphic2.Add(ellipse4)

    graphic.Add(graphic1)
    graphic.Add(graphic2)

    graphic.Print()
  Fin
```
--
### Facade

Permet de masquer tout un gros traitement dernière un appel de fonction
![imgfacade](Img/facade.gif)

```
Simulation de la CAF
```
--
### Proxy

A la meme api que la classe original mais ajout differentes couches de traitement :
- Check de droit d'access
- Stream des données
- Ext

![imgfacade](Img/proxy.gif)

```
Exo 1 security proxy
Calculatrice avec un check sur le level de l'utilisateur
et une partie identification
  public interface IMath
  {
    double Add(double x, double y);
    double Sub(double x, double y);
    double Mul(double x, double y);
    double Div(double x, double y);
  }

```
--
```
Exo 2 
Ecrire un programme qui charge des images du disque :
loadImageFromDisk
displayImage
Les images mettent 1 seconde a ce charger
Ecrivez un programe qui charge toute les images et qui affiche 10 images de cette liste de facon aleatoire
```

--
### Command
Permet de transformer une requête en un object.
ce qui permet de le paramétrer, de l'exécuter et de revenir en arrière.

![Command](Img/command.gif)

```
Exo 1
Ecrire un programe qui permet de faire une liste d'operations sur une lampe
INterupteur : invoker
Lampe : recever
Comande Alumer / eteindre : ConcreteComand
```
```
Exo 2
Faire une calculatrice avec suite d'operation :
1+2+3+4*6-20
(Possibilité de revenir en arriere)
```
```
Exo 3

Une voiture est définie par :
nom
dateStock
Prix
GetDureeStock(entre aujourd'hui dateStock)
ModifierPix par un taux
ToString

Une CommandeDeSolde qui :
permet de solder une liste de voiture qui sont en stock depuis plus de X jours celons un taux
on doit pouvoir annuler / refaire la solde

Un catalogue qui contien une liste de voiture et gere les soldes au besoins

Ecrire un programe qui permet de gerer un catalogue et d'ajouter / annuler des soldes et des voitures.
```

--
### Observer / Observable

- Limitation du couplages fort entre des classes au juste fait d'observer
- A utiliser des qu'une classe doit regagir au changement d'état d'une autre
- Correspond aux Event

--
```
  Observeur = Interface
    Public Fonction Notify()
  FinInterface
```
--
```
  Personnage = Classe
    Var ListeObservateur : tableau [0 à 9] de type Observeur
    Var LastIndex : entier
    Public Fonction AjouterObserveur(ref observeur : Observeur)
      ListeObservateur[LastIndex] = observeur
      LastIndex = LastIndex+1
    FinFonction
    Public Fonction MiseAJourObservateurs() : Null
      Pour i allant de 0 a 9 Faire
        ListeObservateur[i].Notify()
      FinPour
    FinFonction
    Public Fonction Déplacement()
      MiseAJourObservateurs()
    FinFonction
```
--
```
  Logger = Classe implemente Observeur
    Public Fonction Notify()
      Afficher("Je me suis deplacé")
    FinFonction
  FinClasse
```
--
### State

Change le comportement d'un object en fonction de son état.
Un etat met a jour l'object qui contient l'etat avec un nouveau en fonction des conditions

![state](Img/state.gif);

```
Exo 1

Un compte est composé de :
 - un etat
 - un nom d'user
 - une balance
et de methodes
 - deposer
 - retirer
 - payerinteret

un etat est composé de 
d'une balance
d'un taux d'interet
d'une limite max
d'une limite basse
et de methodes
 - deposer
 - retirer
 - payerinteret

faire les etats rouges / silver / gold et quelques operations

```
--
```
Exo 2

Une commande contien :
un etat
une liste de produit
- ajouterProduit
- supprimerProduit
- effacerCommande
- etatSuivant
- CalculerTotal
- to string

Les differents etats de d'une commande sont EnCour / Valider / Livrer
- Encour : on peut ajouter / supprimer / vider / changer d'etat
- Valider : on peut vider / Calculer le total / changer d'etat
- Livrer

Faire un programe qui permet de d'afficher une liste de produit et de faire une commande qui :
En cour : permet d'ajouter / supprimer
Valider : faire payer
Liver : passer en livré apres un certaint temps;
```
--
### Strategie

- Fait de changer le comportement d'une classe à l'exectution
- A utiliser des que le comportement d'une classe doit changer en fonction d'un element exterieur

--

```
  Strategie = Interface
    Private Fonction MettreEnOeuvre() : Null
  FinInterface
```

```
  SeigneurDeGuerre = Classe
    private var strat : Strategie
    Public Fonction SetStrategie(var strategie : Strategie) : Null
      strat = strategie
    FinFonction
    Public Fonction PrendreVille() : Null
      strat.MettreEnOeuvre()
    FinFonction
  FinClasse
```
--
```
  DefoncerPontLevis = Classe implemente Strategie
    Public Fonction MettreEnOeuvre() : Null
      Afficher("Je casse le pont-levi")
    FinFonction
  FinClasse
```

```
  VoyagerDansLeTemp = Classe implemente Strategie
    Public Fonction MettreEnOeuvre() : Null
      Afficher("Je prend ma delorean")
    FinFonction
  FinClasse
```
--
```
var ThulsaDoom : SeigneurDeGuerre
var Meteo : entier
Debut
  ThulsaDoom = new SeigneurDeGuerre
  Meteo = Random(0,1)
  Si Meteo == 0 Alors
    ThulsaDoom.SetStrategie(New DefoncerPontLevis())
  Sinon
    ThulsaDoom.SetStrategie(New VoyagerDansLeTemps())
  FinSi
  ThulsaDoom.PrendreVille()
Fin
```
--

---

## Sources

https://www.dofactory.com/net/design-patterns