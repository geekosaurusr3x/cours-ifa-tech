---
theme : "css/theme/synthwave.css"
revealOptions:
    transition: "zoom"
highlightTheme: "darkula"
separator: ^---$
verticalSeparator: ^--$
---

## Socket

<small>Par [Vincent Lamouroux](https://gitlab.com/geekosaurusr3x)</small>
<small>Cour disponnible en ligne sur [https://geekosaurusr3x.gitlab.io/cours-ifa-tech](https://geekosaurusr3x.gitlab.io/cours-ifa-tech)</small>

---
## C'est quoi ?

- Moyen de communiquer avec les autres sur un réseau
- Peut être représenté par un tuyau
- Agit au niveau applicatif du model TCP/IP et Osi
  
---
## Model TCP/IP et Osi
---
![modelIP](Img/modeltcp-ip.png)
--
- TCP/IP ancien et a fait ces preuves
- Osi est plus robuste mais plus complexe à mettre en place
- Chaque couche est indépendantes
--
### Physique

- Hardware
- Gère la réception et l'envoi des bits
- On parle de bits

--
### Liaison de données

- Adressage en mac
- Adresse physique
- Protocole bas niveaux
 - Ethernet
 - ARP
 - WI-FI
- On parle de trames
--
### IP

- La couche réseau
- Gère l'adresse logique
  - IP V4 : 127.0.0.1
  - IP V6 : 0000:0000:0000:0000:0000:0000:0000:0000
- On parle de paquets

--
### TCP / UDP

- La couche qui découpe vos objects pour en faire des paquets
- Contrôle l'envoi et la réception des bouts d'object
- On parle de segments
- Gère les ports :
  - 0 à 65536
  - < 1024 réservé a des application enregistré
  - => 1024 c'est bon
  
Note:
- IANA
--
### Applicatif

- DHCP
- VOUS
- On utilise des protocoles de communication
- On parle d'objets

---
### UPD
#### User Datagram Protocol

- Pas de connections
- Pas de vérification des données (La couche applicatif s'en charge)
- Pas de garantie dans l'ordre d'arrivé des paquets
- Protocole très rapide
- C'est la poste
---
### TCP
#### Transmission Control Protocol

- Connections
- Garantie l'integrité du paquets envoyé
- Garantie l'ordre d'arrivé des paquets
- Protocole plus lent
- C'est transport de font

---
### Un peut de code UDP
--
### Client

``` C#
    using System.Net.Sockets;
    
    byte[] msg = Encoding.Default.GetBytes(message);

    UdpClient udpClient = new UdpClient();

    udpClient.Send(msg, msg.Length, "127.0.0.1", 1976);

    udpClient.Close();
```
--
### Server
``` C#
    using System.Net.Sockets;
    UdpClient udpServer = new UdpClient(new IPEndPoint(IPAddress.Any,1976));

    while (true)
    {
        IPEndPoint EnDpoint = null;
        byte[] data = udpServer.Receive(ref EnDpoint);

        string message = Encoding.Default.GetString(data);
    }
```
---
### Un peut de code TCP
--
### Client

``` C#
    using System.Net.Sockets;
    
    byte[] msg = Encoding.Default.GetBytes("je suis de la data");

    TcpClient tcpclnt = new TcpClient();
    tcpclnt.Connect("127.0.0.1",8001);
    NetworkStream stm = tcpclnt.GetStream();
    stm.Write(msg,0,msg.Length);

    tcpclnt.Close();
```
--
### Server
``` C#
    using System.Net.Sockets;
    IPEndPoint EnDpoint = new IPEndPoint(IPAddress.Any,8001);
    TcpListener myList=new TcpListener(EnDpoint);
    myList.Start();
    while (true)
    {
        Socket s=myList.AcceptSocket();

        byte[] data=new byte[100];
        int k=s.Receive(data);

        string message = Encoding.Default.GetString(data);
        s.Close();
    }
    myList.Stop();
```
---
### Details

- Les opérations sur les sockets sont bloquantes
- Un port non fermé, restera inaccessible jusqu'au reboot;

---
## Sources
[Programmation reseau C#](https://openclassrooms.com/fr/courses/379507-la-programmation-reseau-en-ne t/379088-la-theorie-de-depart)