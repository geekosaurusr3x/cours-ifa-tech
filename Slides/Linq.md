---
theme : "css/theme/synthwave.css"
revealOptions:
    transition: "zoom"
highlightTheme: "darkula"
separator: ^---$
verticalSeparator: ^--$
---

## Language-Integrated Query
### (LINQ)

<small>Par [Vincent Lamouroux](https://gitlab.com/geekosaurusr3x)</small>
<small>Cour disponnible en ligne sur [https://geekosaurusr3x.gitlab.io/cours-ifa-tech](https://geekosaurusr3x.gitlab.io/cours-ifa-tech)</small>

---

- Language de requêtes.
- Langage unique :
  - SQL pour les bases DB
  - Xquery pour le XML
- Travail sur des collection d'objets (IEnumerables)

---

### Utilisation

--
```C#
using System.LINQ;
// 1. Data source.
int[] numbers = new int[7] { 0, 1, 2, 3, 4, 5, 6 };

// 2. Query creation.
IEnumerable<int> numQuery =
    from num in numbers
    where (num % 2) == 0
    select num;

// 3. Query execution.
foreach (int num in numQuery)
{
    Console.Write(num);
}
```

--

```C#
// 1. Data source.
int[] numbers = new int[7] { 0, 1, 2, 3, 4, 5, 6 };
```

Definition d'un Ienumerable comme source de données

--

```C#
// 2. Query creation.
IEnumerable<int> numQuery =
    from num in numbers
    where (num % 2) == 0
    select num;
```

Definition d'une requête

--

```C#
// 3. Query execution.
foreach (int num in numQuery)
{
    Console.Write(num);
}
```
Exécution de la requête.

---

### Définition

--
#### Selection des données

--
```c# 
IEnumerable<int> numQuery =
    from num in numbers
```

- definition de la source sur laquelle on opère
- definition de variable utilisé dans la requête
- similaire au foreach
  - Imbriquables

--
```c# 
IEnumerable<int> numQuery =
    from num in numbers
    select num
```

- definition des données de retour
- nom de variable
- expression
- nouvelle structure de données
--
```c#
var numQuery =
    from num in numbers
    select new {att1 =  num, att2 = num}
```
--
```c# 
IEnumerable<int> numQuery =
    from num in numbers
    where num < 5
    select num
```
- Définition de la condition
- Operation booleene
--
```c# 
IEnumerable<int> numQuery =
    from num in numbers
    where num < 5
    orderby num ascending
    select num
```
- Définition d'un ordonnancement
- ascending
- descending
- plusieurs variables pour plusieurs tris
---

#### Syntax Query / Syntax Methods

--

```c# 
IEnumerable<int> numQuery =
    from num in numbers
    where num < 5
    select num
```

```c#
IEnumerable<int> query =
    numbers.Where((num) => num < 5);
```

---

#### Conversion de types

--

ToArray()

```c#
    int[] numQuery =
    (from num in numbers
    where num < 5
    select num).ToArray();
```

```c#
    int[] numQuery =
    numbers.Where((num) => num < 5).ToArray();
```
--

ToList()

```c#
    List<int> numQuery =
    (from num in numbers
    where num < 5
    select num).ToList();
```

```c#
    List<int> numQuery =
    numbers.Where((number) => number < 5).ToList();
```
--

ToDictionary()

```c#
    Dictionary<string,int> numQuery =
    (from num in numbers
    where num < 5
    select num).ToDictionary(key=>key.ToString());
```

```c#
    Dictionary<string,int> query =
    numbers.Where((num) => num < 5).ToDictionary(key=>key.ToString());
```
---
#### Agregation

--

Count => Nombre d'elements
```c#
    int query =
    numbers.Where((number) => number < 5).Count();
```
--

Max => Valeure maximale
```c#
int query =
    numbers.Where((number) => number < 5).Max();
```
--

Min => Valeure minimale
```c#
    int query =
    numbers.Where((number) => number < 5).Min();
```

--

 Sum => Somme de tous les elements
```c#
    int query =
    numbers.Where((number) => number < 5).Sum();
```

--

 Average => Moyene de tous les elements
```c#
    float query =
    numbers.Where((number) => number < 5).Average();
```
---

#### Regroupement et Ensembles

--
 Group By

 ![image](Img/linq_group.png)

--

```c#
    List<int> numbers = new List<int>(){1,2,3,4,5,6,7,8,9};

    IEnumerable<IGrouping<int, int>> query = from number in numbers  
                                         group number by number % 3;
    foreach (IGrouping<int, int> group in query)  
    {  
        Console.Write(group.Key+" : ");  
        foreach (int i in group){
            Console.Write(i);
            Console.WriteLine();
        }

    }  
```

--
 Distinct

 ![image](Img/distinct-method-behavior.png)

--

```c#
    List<int> numbers = new List<int>(){1,1,1,2,2,2,2,3,4,5,5,5,6,7,8,9};

    IEnumerable<int> query = (from number in numbers  
                                            select number).Distinct();
    foreach (int value in query)  
    {  
        Console.WriteLine(value);  
    }  
```

--
 Except

 ![image](Img/except-behavior-graphic.png)

--

```c#
    List<int> numbers = new List<int>(){1,1,1,2,2,2,2,3,4,5,5,5,6,7,8,9};
    List<int> other = new List<int>(){1,2,6,7,8,9};
    IEnumerable<int> query = (from number in numbers  
                                            select number).Except(other);
    foreach (int value in query)  
    {  
        Console.WriteLine(value);  
    }  
```

--
 Intersect

 ![image](Img/intersection-two-sequences.png)

--

```c#
    List<int> = new List<int>(){1,2,3,4,5,6,7,8,9};
    List<int> other = new List<int>(){1,2,6,7,8,9};

    IEnumerable<IGrouping<int, int>> query = (from number in numbers  
                                            select number).Intersect(other);
    foreach (int value in query)  
    {  
        Console.WriteLine(value);  
    } 
```

--
 Union

 ![image](Img/union-operation-two-sequences.png)

 /!\ Effectue un Distinct()
--

```c#
    List<int> = new List<int>(){1,2,3,4,5,6,7,8,9};
    List<int> other = new List<int>(){1,2,6,7,8,9};

    IEnumerable<IGrouping<int, int>> query = (from number in numbers  
                                            select number).Union(other);
    foreach (int value in query)  
    {  
        Console.WriteLine(value);  
    } 
```
---

#### Jointure

--

Mots clef pour joindre 2 sources de données entre elle en se basant sur un element commun

- join
- Group join

![jointure](Img/join-method-overlapping-circles.png)

--

join

- Inner Join
- Equivalent de plusieur `from into`

--

```c#
    List<int> numbers = new List<int>(){1,2,3,4,5,6,7,8,9};
    List<int> other = new List<int>(){1,2,6,7,8,9};

    IEnumerable<int> query = from number in numbers
                             from number2 in other
                             where number == number2
                             select number
```

```c#
    List<int> numbers = new List<int>(){1,2,3,4,5,6,7,8,9};
    List<int> other = new List<int>(){1,2,6,7,8,9};

    IEnumerable<int> query = from number in numbers
                             join number2 in other on number equals number2
                             select number
```

--

Group Join

- Left Outer Join

--

```c#
    var query = from p in Parent
                join c in Child on p.Id equals c.Id into g
                select new { Parent = p, Children = g }
```
--

#### Comparaison des join

--

```
     Join                    Group Join
Value ChildValue         Value ChildValue
A     a1                 A    [a1, a2, a3]
A     a2                 B    [b1, b2]
A     a3                 C    []
B     b1                  
B     b2                  
```

---
### Exos
--
#### Exo 1
Ecrivez des requêtes qui sortent:
- tous les nombres pair
- tous les multiples de 5
- multiplie par 10 tous les résultats
d'un tableau d'entier,
--
#### Exo 2
Écrivez un programme qui contiens un deck de 54 cartes melangé
une carte contiens une couleur et une valeur

Ecrivez les requetes pour trouvez :
- toute les cartes d'une certaine couleur
- toutes les cartes > 10
- toutes les cartes classées par couleur et par valeur
--
#### Exo 3
Ecrivez une programme qui permet de :

- chercher un étudiant par un nom
- Afficher toute les notes qui lui correspondent
- Afficher tous les étudiants qui ont une note en dessous de la moyenne
- Chercher tous les étudiants qui ont un prof;
--
#### Exo 4
Soit une une liste d'employé (nom,id,poste,ancieneté)
Soit une liste de fiche de paye (id,nom,nbheure,nbheure-sup,salaire)

ecrivez les requetes pour :
- touvez le salaire moyen
- toute les fdp d'un employer
- ceux qui ne font par d'heure sup
- l'ancieneté moyene
- la liste des salariés ordoné par ancienté coissante
--
#### Exo 5
Simultation de bibliotheque
Livre : id, nom, idAuteur,nbpages,datepublication, emprunté,nbemprunts
Auteur : id, prenom,nom,datedenaissance
utilisateur : id, nom,prenom,listelivreemprunté

Faire une facade bibliotheque pour :
- s'identifier
- demander les noms des livres emprunté par un utlisateur
- demander la bibliographie d'un auteur
- Emprunter un livre (pas impruntable 2 fois)
  -  pas impruntable 2 fois 
  -  Si on a deja un emprunter on peut pas
-  Le classement des auteur par emprunts
--
#### Exo 6
Soit une liste de monstres (nom,id,race,points d'attaques,points de vie)
Soit une liste de heros (id,nom,race,arme)
Soit une liste de combat(idhero,idmonstre,list de {val degas,elements} hero,list de {val degas,elements} monstre,victoire hero ou monstre)

ecrivez les requetes pour :
- touvez les dégas moyens
- La race de monstre qui combat le plus
- l'arme qui fait les plus gros degas moyens
- L'element qui fait le plus de degas
- Le nom du hero qui fait le plus de degas
--
#### Exo 7
Dans fichiez texte :
- selectionez toute les lignes qui commence par un a
- selectionez toutes lettres communes à tous les mots dans une phrase aleatoire
- calculez la moyene des lettres par lignes
- calclulez le nombre de voyelles par ligne

--
#### Exo 8
Simulation d'un site d'achat
client(id nom email ville)
panier(id produit quantité)
produit(id nom prix)

Selectionez tous les clients qui ont acheté un produit groupé par produit
Calculez le montant moyen du panier
Calculez la ville qui commande le plus

--
#### Exo 9

Un momument est defini par id,nom,idville,une liste de {jour,nbvisiteur}
Une ville est definie par id,nom,pays
Un panier est composé d'un id,d'une liste de produit,d'un total
Un produit est composé d'un id,d'un id de momnument et d'un nom, prix
Ecrivez les fabriques
Ecrivez les requetes pour:
- Calculer la moyenne de visite pour un monument.
- Calcule le nombre de visiteur entre 2 dates
- Trouvez tous les monuments pour un pays
- Ordoner les momuments en les classant par ville
- Calculez le panier moyen
- Calculez le prix moyen d'un produit acheté
- Trouvez tous les achats pour une une ville et une date donnée

--
#### Exo 10

Un hopital est defini par id,nom,idville
Une ville est definie par id,nom,pays
Un dossier medical est defini par un numdesecu,une liste d'actesmedicaux
Un acte medical est définit par un id,un idhopital,une date, un titre et montant
Un patien est definit par un numsecu, un nom,un prenom,un date de naissance,un sexe
Ecrivez les fabriques
Ecrivez les requetes pour:
- Calculer la moyenne d'hospitatlisation pour un hopital donne.
- Calcule le nombre d'intervention medicale entre 2 date pour un hopital donné
- Classer les hopitaux par recetes, affichez le nom et la recete
- Sortiez la maladie qui est le plus traité
- Calculez par ville, le sexe le plus soigné
- Trouvez le pays avec le plus d'hopital
- Trouvez entre 3 hopitaux la liste des interventions communes

---
## Sources
[DOC MSDNA](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/linq/introduction-to-linq-queries)
[StackOverflow Join vs GroupJoin](https://stackoverflow.com/questions/15595289/linq-to-entities-join-vs-groupjoin)