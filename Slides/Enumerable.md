---
theme : "css/theme/synthwave.css"
revealOptions:
    transition: "zoom"
highlightTheme: "darkula"
separator: ^---$
verticalSeparator: ^--$
---

## Enumerable

<small>Par [Vincent Lamouroux](https://gitlab.com/geekosaurusr3x)</small>
<small>Cour disponnible en ligne sur [https://geekosaurusr3x.gitlab.io/cours-ifa-tech](https://geekosaurusr3x.gitlab.io/cours-ifa-tech)</small>

---

- Permet de transformer une classe en une structure parcourable
- List
- Tableau
- Utilisation :
  - foreach
  - requête

---

- Ienumerator
- Ienumerable

---
### Ienumerator

--

- Classe
- Représente l'élément qui itère
- Implémente Ienumerator<T>

--
```c#
    class totoEnumerator : IEnumerator<T>
    {
        public T Current => retourne l'enement courent;

        object IEnumerator.Current => retourne l'enement courent;

        public void Dispose() { libère les resources si nécessaire}

        public bool MoveNext(){ Permet de passer a l'element suivant }

        public void Reset() { remet de se repositionner au debut }
```
---

### Ienumerable

--

- Classe
- Représente l'élément sur lequel on itère (la collection)
- Implémente Ienumerable<T>

--

```c#
    class toto : Ienumerable<T>
    {
        public IEnumerator<T> GetEnumerator()
        {
            retourne un nouvel Enumerable;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            retourne un nouvel Enumerable;
        }
    }
```

---

### Bonus Stage : [] Indexer

--

- Permet d'utiliser les [] comme notation
- Souvent associé à une méthode Add(T)

--

```c#
   public T this[int i]
   {
      get { return élément à l'index i; }
      set { élément à l'index i = value; }
   }
```

---

### Bonus Stage : <T> Type Generique

--

Permet d'utiliser n'imposte quel type dans sa collection

--

```c#
    class toto<T> : Ienumerable<T>
    {
        public IEnumerator<T> GetEnumerator()
        {
            return new totoEnumerator<T>();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new totoEnumerator<T>();
        }
    }
```
--
```c#
    class totoEnumerator<T> : IEnumerator<T>
    {
        public T Current => retourne l'enement courent;

        object IEnumerator.Current => retourne l'enement courent;

        public void Dispose() { libère les resources si nécessaire}

        public bool MoveNext(){ Permet de passer a l'element suivant }

        public void Reset() { remet de se repositionner au debut }
```
--
```c#
    toto<string> test = new toto<string>();
```
