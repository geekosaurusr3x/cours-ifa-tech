---
theme : "css/theme/synthwave.css"
revealOptions:
    transition: "zoom"
highlightTheme: "darkula"
separator: ^---$
verticalSeparator: ^--$
---

# Entity Framework

<small>Par [Vincent Lamouroux](https://gitlab.com/geekosaurusr3x)</small>
<small>Cour disponnible en ligne sur [https://geekosaurusr3x.gitlab.io/cours-ifa-tech](https://geekosaurusr3x.gitlab.io/cours-ifa-tech)</small>

---

## Presentation

--

- Orm (Object / Relation Mapper)
- Permet manipuler une basse de donnée avec des classes

--

Instalation avec nuget

![install](Img/managenugetpackages.png)

---

## Utilisation

--

- Utilisation d'une classe pour reprenster une entitée
- Utilisation d'un BD context pour :
  - Ecrire et executer les requetes
  - Representer les resultas
  - Traquer et persister les changements

--

### Representation des données

--

```c#
using System.Data.Entity;
```

```c#
    class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
```

```c#
    class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }
```

--
### Connection à une base de donnée specifique

- Utilisation d'un fichier app.config
- Ajouter -> Nouvel Element -> Fichier de Configuration d'Application
- Doit contenir

```
<configuration>
  <connectionStrings>
    <add name="Nom Context" connectionString="Data Source=127.0.0.1; Initial Catalog = exo6test ; Intgrated Security = no" providerName="System.Data.SqlClient" />
  </connectionStrings>
</configuration>
```
--
```c#
    class ApplicationContext : DbContext
    {
        public ApplicationContext() : base("name=Nom context"){ }

        public DbSet<User> Users { get; set; }
    }

    new ApplicationContext(con);
```
--

### Lecture des données

```c#
ApplicationContext db = new ApplicationContext()
var query = from u in db.Users
            orderby u.Name
            select u;
```

--

- Find() : permet de chercher par ID

```c#
ApplicationContext db = new ApplicationContext()
User user = db.Users.Find(1);
``
--

### Ecriture des données

```c#
ApplicationContext db = new ApplicationContext()
User user = new User { Name = name };
db.Users.Add(user);
db.SaveChanges();
```

--
### Evolution du model

```c#
    class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string prenom { get; set; }
    }
```
--

Dans la console du Gesionaires de package :

```c#
Enable-Migrations
```

```c#
Add-Migration AddPrenom
Update-Database
```

--
### Visualisation des données

![install](Img/schemawithurl.png)
--
### Annotation

permet de définir le comportement par rapport à la base

-- 

Namespace 
```
    using System.ComponentModel.DataAnnotations;
```

--

- [key] : permet de fenir une clef primaire
- Peut etre utilisé plusieur fois mais necessite l'annotation [Column(Order = X)]

--

```c#
    class User
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string prenom { get; set; }
    }
```

```c#
    class User
    {
        [Key]
        [Column(Order = 1)]
        public int Id { get; set; }
        [Key]
        [Column(Order = 2)]
        public string Name { get; set; }
        public string prenom { get; set; }
    }
```

--

- [Required] : empêche le champ d'être vide

```c#
    class User
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string prenom { get; set; }
    }
```
--

- [NotMapped] : empeche la propriété d'etre generé dans la base
- Typiquement utilisé pour les proprieté calculées

```c#
    class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string prenom { get; set; }
        [NotMapped]
        public string FullName{
            get { return prenom+" "+Name;}
        }
    }
```
--

- [Table("MesUsers")] : permet de changer le nom de la table
- [Column("Prenoms")] : premet de changer le nom du champ
```c#
    [Table("MesUsers")]
    class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Column("Prenoms")]
        public string prenom { get; set; }
    }
```
--

- [Index] : premet de crée un index sur un champ
- Peut avoir un nom : "MyIndexName"
- Peut etre unique avec : "IsUnique = true"
- Peut etre composé de plusieur champ (et ordoné) ",X"

```c#
    class User
    {
        public int Id { get; set; }
        [Index("IX_Nom_Prenom",1)]
        public string Name { get; set; }
        [Index("IX_Nom_Prenom",2)]
        public string prenom { get; set; }
    }
```
--
- [Index] : premet de crée un index sur un champ
- Peut avoir un nom : "MyIndexName"
- Peut etre unique avec : "IsUnique = true"
- Peut etre composé de plusieur champ (et ordoné) ",X"

```c#
    class User
    {
        public int Id { get; set; }
        [Index("IX_Nom_Prenom",1)]
        public string Name { get; set; }
        [Index("IX_Nom_Prenom",2)]
        public string prenom { get; set; }
    }
```

---
### Relations
--
#### Relation 1
- [ForeignKey("field")] : permet de definir la clef etrangere d'un champ de navigation

```c#
    using System.ComponentModel.DataAnnotations;
    public class Article {
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        virtual public User Auteur { get; set; }
    }

    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string prenom { get; set; }
    }
```
--
#### Relation N
```c#
    using System.ComponentModel.DataAnnotations;
    public class Article {
        public int Id
    }

    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string prenom { get; set; }
        virtual public List<Article> ArticleWritten { get; set; }

    }
```
--
#### Propriété de deplacement
- [InverseProperty("")] : permet de définir la clef étrangère lorsque y à plusieurs relations entre 2 tables

```c#
    using System.ComponentModel.DataAnnotations;
    public class Article {
        public int Id
        virtual public User CreatedBy { get; set; }
    }

    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string prenom { get; set; }
        public int ArtID { get; set; }
        [InverseProperty("CreatedBy")]
        virtual public List<Article> ArticleWritten { get; set; }

    }
```
--
#### Relation 1,N

mix des 2
--
#### Relation N,N

```c#
    using System.ComponentModel.DataAnnotations;
    public class Article {
        public int Id { get; set; }
        public string Content { get; set; }
    }

    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string prenom { get; set; }
    }
```
--
```c#
    public class ArticleUser {
        [Key]
        [Column(Order = 0)]
        public int ArticleId { get; set; }
        [ForeignKey("ArticleId")]
        virtual public Article Article { get; set; }

        [Key]
        [Column(Order = 1)]
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        virtual public User User { get; set; }
        
public DateTime WriteDate { get; set; }
    }
```

---
## Sources
[DOC MSDNA](https://docs.microsoft.com/en-us/ef/ef6)