---
theme : "css/theme/synthwave.css"
revealOptions:
    transition: "zoom"
highlightTheme: "darkula"
separator: ^---$
verticalSeparator: ^--$
---

## Test / Versionning de code

<small>Par [Vincent Lamouroux](https://gitlab.com/geekosaurusr3x)</small>
<small>Cour disponnible en ligne sur [https://geekosaurusr3x.gitlab.io/cours-ifa-tech](https://geekosaurusr3x.gitlab.io/cours-ifa-tech)</small>

---
## Versionning de code

--
### A quoi ca sert

A eviter :
- « Qui a modifié le fichier X, il marchait bien avant et maintenant il provoque des bugs ! » ;
- « Qui a ajouté cette ligne de code dans ce fichier ? Elle ne sert à rien ! » ;
- « À quoi servent ces nouveaux fichiers et qui les a ajoutés au code du projet ? » ;
- « Quelles modifications avions-nous faites pour résoudre le bug de la page qui se ferme toute seule ? »

--
## C'est quoi

Simplement le faire d'utiliser un / des outils qui permette :
- A tout instant de revenir a une certaine version du code
- Fusionner du travail collaboratif

--
## Un peut d'histoire
(Dans l'ordre d'apparition l'écran)
- CVS
- SVN
- Git
- Mercurial
- Bazaar
- ...

--
## Centraliser / decentraliser

- Centralisé ( CVS / SVN )
  - Un serveur central contient l'historique
  - On ne travail que sur la dernière version
  - On ne communique qu'avec le serveur central

- Décentralisé ( Git / Mercurial / Bazaar)
  - Chacun à l'historique complet
  - On communique ensemble et très souvent
  
--
## Comment ca fonctionne
- On fait des modifications sur les fichiers
- On les sauvegarde dans le gestionnaire :
  - Sur le serveur central : CVS / SVN
  - En local : Git / Mercurial
- Et on recommence
- Quand on a finit de travailler on envois au autres :
  - Deja fait : CVS / SVN
  - On doit envoyer : Git / Mercurial

--
## Git

- On travail sur des branches séparées :
  - Master  = code stable
  - Develop = code en travail
- Quand on a finit un fichier de code :
  - On le sauvegarde
  - On Add avec `git add monfichiersource.txt`
  - On Commit avec `git commit -m "Mon nouveau code"`
  - Si on partage avec des gents `git push`

--
## Hébergeur de code

- Service tier permet de faire une sauvegarde en ligne (Complement dispensable)
- Github
- Gitlab
- Bitbucket

--
## Comment bien versionné son code

voir Source GitFlow

---
## Sources

- [Test unitaire java](https://openclassrooms.com/fr/courses/1301341-les-tests-unitaires-en-java)
- [Gérez vos codes source avec Git](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)
- [Git Flow](https://nvie.com/posts/a-successful-git-branching-model/)