---
theme : "css/theme/synthwave.css"
revealOptions:
    transition: "zoom"
highlightTheme: "darkula"
separator: ^---$
verticalSeparator: ^--$
---

## Anonyme and Lambda

<small>Par [Vincent Lamouroux](https://gitlab.com/geekosaurusr3x)</small>
<small>Cour disponnible en ligne sur [https://geekosaurusr3x.gitlab.io/cours-ifa-tech](https://geekosaurusr3x.gitlab.io/cours-ifa-tech)</small>

---

### Méthodes Anonymes

--

- Sert a raccourcir son code
- Plus rapide de à l'exécution
- N'est pas lier à un identificateur
- Juste a une signature
- s'appel `delegate`

--

```c#
// definition du delegate en dehors d'une fonction
delegate void Del(int x);

//Instantiation du delegate
Del d = delegate(int k) { /* ... */ };

//Utilisation
d(10);
```

--

- Les variables extérieur sont capturées (connues)
- Un delegate peut pointer sur une fonction

---

### Lamba

--

#### Expressions

- Sert a untiliser une expression comme un object
- Principalement utilisé pour du LINQ

--

```
Action< (0 -  n) type de var > Nom = (nom des var) => expression
Func< (1 - n) type de var,  type de sortie > Nom = (nom des var) => expression
```

```c#
Func<int, bool> positive = (x) => x > 0;
Console.WriteLine(positive(5));
```

--

#### Statement

- La meme chose qu'avant mais sur plusieurs lignes
- Statement entourée par {}
--

```c#
Action<string> greet = name =>
{ 
    string greeting = $"Hello {name}!";
    Console.WriteLine(greeting);
};
greet("World");

```

---