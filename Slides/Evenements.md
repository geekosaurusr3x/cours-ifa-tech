---
theme : "css/theme/synthwave.css"
revealOptions:
    transition: "zoom"
highlightTheme: "darkula"
separator: ^---$
verticalSeparator: ^--$
---

## Gestion des evenements

<small>Par [Vincent Lamouroux](https://gitlab.com/geekosaurusr3x)</small>
<small>Cour disponnible en ligne sur [https://geekosaurusr3x.gitlab.io/cours-ifa-tech](https://geekosaurusr3x.gitlab.io/cours-ifa-tech)</small>

---

- Extension du DP Observeur/Observable
- Composé de deux / trois elements :
  - Un emeteur
  - Un recepteur
  - Des parametres

---

### Emeteur
- Classe qui va emetre l'evenement
- Contien une reference vers le recepteur

--
```
    class Counter
    {
        int threshold;
        int total;
        public event EventHandler ThresholdReached;

        public Counter (int max){
            threshold = max;
            total = 0;
        }

        public void Add(int x)
        {
            total += x;
            if (total >= threshold)
            {
                ThresholdReached(EventArgs.Empty);
            }
        }
        
        protected virtual void ThresholdReached(EventArgs e)
        {
            EventHandler handler = ThresholdReached;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
```

---
### Recepteur
- Classe qui va recevoir l'emeteur

--
```
    class CounterRecepteur
    {
        public void ThresholdReached(object sender, EventArgs e)
        {
            Console.WriteLine("The threshold was reached.");
        }
    }
```
---
### Utilisation
--
```
    static void Main(string[] args)
    {
        Counter c = new Counter(new Random().Next(10));
        c.ThresholdReached += OnThresholdReached;
        for (int i = 0; i <= 20; i++){
            c.Add(1);
        }
    }

    static void OnThresholdReached(object sender, EventArgs e)
    {
        Console.WriteLine("The threshold was reached.");
    }
```
---
### Des données
- Classe qui va contenir les données de l'event

--
```
    public class ThresholdReachedEventArgs : EventArgs
    {
        public int Threshold { get; set; }
        public DateTime TimeReached { get; set; }
    }
```
--
```
    class Counter
    {
        public event EventHandler<ThresholdReachedEventArgs> ThresholdReached;

        public void Add(int x)
        {
            total += x;
            if (total >= threshold)
            {
                ThresholdReachedEventArgs args = new ThresholdReachedEventArgs();
                args.Threshold = threshold;
                OnThresholdReached(args);
            }
        }

        protected virtual void OnThresholdReached(EventArgs e)
        {
            EventHandler handler = ThresholdReached;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
```
--
```
    class CounterRecepteur
    {
        public void ThresholdReached(object sender, ThresholdReachedEventArgs e)
        {
            Console.WriteLine("The threshold "+e.Threshold+"was reached at"+e.TimeReached);
        }
    }
```
---

## Sources

[Gestion des evemements](https://docs.microsoft.com/fr-fr/dotnet/standard/events/)