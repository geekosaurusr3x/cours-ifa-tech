---
theme : "css/theme/synthwave.css"
revealOptions:
    transition: "zoom"
highlightTheme: "darkula"
separator: ^---$
verticalSeparator: ^--$
---

## Threading

<small>Par [Vincent Lamouroux](https://gitlab.com/geekosaurusr3x)</small>
<small>Cour disponnible en ligne sur [https://geekosaurusr3x.gitlab.io/cours-ifa-tech](https://geekosaurusr3x.gitlab.io/cours-ifa-tech)</small>

---
### Processus

- Instance d'un programme en cour d'exécution
- Propre espace mémoire
- Isolé des autres processus

Note:

Un processus est en réalité un programme en cours d’exécution. Lorsque vous
exécutez votre programme C#, par exemple, un processus est lancé. Vous
pouvez voir un processus comme une ‘instance’ de votre programme.
Chaque processus possède son propre espace mémoire, du coup les processus
ne se ‘chevauchent’ pas. Vous pouvez donc exécuter plusieurs fois votre
programme C#. Chaque exécution de votre programme sera alors interprété
comme un processus différent.
Les processus sont des tâches lourdes.

---
### Multi-tâches (multi-tasking)

- Plusieurs processus en //
- Tous les os modernes
- Partages des resources
- Contiens les threads

Note:
	
Le multitâche signifie qu’on peut lancer plusieurs processus en même temps.
Par exemple, Windows est un système multitâches puisqu’il permet d’exécuter
plusieurs tâches/processus en même temps : vous pouvez écouter une musique
sur internet tout en retouchant une photo sur un logiciel de retouche photo.
Généralement, tous les processus partagent les mêmes ressources (comme le
processeur). Chaque processus partage un espace pour que tous les processus
soient exécutés en parallèle. Cependant, si vous disposez d’une machine (PC)
avec plusieurs processeurs, vous pouvez répartir chaque processus sur chaque
processeur pour gagner en puissance et rapidité.
Un processus peut contenir plusieurs threads

---
### Thread

- Sous tache d'un processus
- Tache légère
- Tourne en // avec le thread principal (Main)
- Deux Types :
  - Foreground
  - Background

Note:

Qu’est-ce qu’un Thread ?
Un Thread est une sous-tache d’un processus.
Un processus peut donc contenir plusieurs threads, chaque thread partageant la
mémoire du processus.
Les processus sont des tâches légères.
Il existe deux types de thread : le thread utilisateur et le thread démon.
Nous y reviendrons un peu plus tard.

---
### Multi-threading

- Possibilité de diviser des opérations / taches en plusieur threads
- Repartition des resources du processus entre les threads
  
Note:

La notion de multi-threading vient du fait qu’il est possible de diviser des opérations/tâches spécifiques au sein d’une application (processus) en plusieurs threads.  
Chacun de ces threads peut être exécuté en parallèle. L’O.S. va diviser le temps de traitement entre les différents processus mais également entre chaque thread.
Le multi-threading permet à plusieurs tâches de s’exécuter simultanément dans le même programme (process).

---
### Cycle de vie d’un Thread
	
Il existe 5 principales étapes dans le cycle de vie d’un thread (au total):

--
1. Unstarted : C’est le début de la vie du Thread. Il reste dans cet état tant que le
programme ne démarre pas le Thread.	
--
2. Runnable : Après avoir été crée, le Thread est maintenant runnable
(opérationnel). Le thread peut être lancé avec la méthode start().	
--
3. Running : Quand le thread est executé, l’état est ‘running’. Il est alors en
exécution dans l’application.	
--
4. WaitSleepJoin  : C’est l’état dans lequel se trouve le Thread lorsqu’il doit attendre. Par
exemple, lorsque deux threads s’exécutent simultanément, il faut attendre que
l’autre thread effectue une tâche.	
--
5. Stopped  : Quand le Thread a fini sa tâche, il est ‘terminé’ (mort).	

---
### Priorités

Il existe 5 priorité d'exécutions :

- Lowest
- BelowNormal
- Normal
- AboveNormal
- Highest
- RealTime

Plus elle est élevée, plus il y a de resources allouées à CE thread.

---
### Fore VS Back

- Foreground = L'application attend la fin de tous les threads
- Background = L'application quitte immédiatement à la fin du main

---
### Lancement d'un thread
```
    Using System.Threading; 

    Thread t = new Thread (Nom de la methode a runner);
    t.Start();  
```
Note:
Exo 1 et 2

---
### Syncronisation inter-thread

- Ultra frequent
- Dans les cas de partage de resources :
  - Variables
  - Fichiers
  - List
--
#### Join
--
Permet d'attendre qu'un thread se finisse avant de continuer
```
    Thread t = new Thread ();
    t.Start();
    t.Join();
```
--
#### Lock
--
```
class Test
{
  static int _val1 = 1, _val2 = 1;
 
  static void Go()
  {
    if (_val2 != 0) Console.WriteLine (_val1 / _val2);
    _val2 = 0;
  }
}
```
--
- Utilisation d'un object static que l'on lock
- Encapsulation du code a locker par  `lock(monlock){}`
  - Sert a capturer un object pour soit
  - Fait passer les autres thread en `SleepWaitJoin` state
- /!\ Ne limite l'accès que lorsque que l'on utilise lock.
--
```
class TestSafe
{
  static readonly object _locker = new object();
  static int _val1, _val2;
 
  static void Go()
  {
    lock (_locker)
    {
      if (_val2 != 0) Console.WriteLine (_val1 / _val2);
      _val2 = 0;
    }
  }
}
```
--
#### Monitor
- Autre ecriture de Lock
  - Monitor.Enter
  - Monitor.Exit
--
```
class ThreadSafe
{
  static readonly object _locker = new object();
  static int _val1, _val2;
 
  static void Go(){
    Monitor.Enter (_locker);
    try
    {
        if (_val2 != 0) Console.WriteLine (_val1 / _val2);
            _val2 = 0;
    }
    finally { Monitor.Exit (_locker); }
  }
}
```
--
#### Choisir son Object de syncronisation

- commun aux threads
- private
- de type reference :
  - Instance d'object
  - Static
--
#### Mutext
- Comme les lock
- Interprocess
- Classiquement utilisé n'avoir qu'une seule instance de programme
--
```
private static Mutex mut = new Mutex();
if(mut.WaitOne()){

}
mut.ReleaseMutex();
```
```
private static Mutex mut = new Mutex(true,"MyNameApp");
if(mut.WaitOne()){
  
}
mut.ReleaseMutex();
```
--
#### Semaphore
- Resemble à une boite de nuit
- Permet de limiter le nombre de thread concurent pour le meme bout de code
- Thread agnostique (n'importe quel thread peut release un thread)
--
```
static SemaphoreSlim _sem = new SemaphoreSlim (3);
_sem.Wait();
_sem.Release();
```
---
### Task
--

- Version Moderne des threads
- Peut prendre un parametre
- Peut avoir une valeur de retour
- "Plus Facile"

--
#### Definition

--
##### Inline avec une Lambda Function;
```C#
new Task<int>(()=>{ 
    //do some stuff
    return 20
});
```
--
##### Une fonction : async Task<int>

```C#
async Task<int> MyTask(int value)
{
  //do some stuf
  return 20
}
```
--
#### Utilisation

```C#
static void Main(string[] args)
{
  Task<int> task = MyTask(int value) //= Task.Run( () => {});
  // si defintion inline
  // task.Start();

  int nb = task.Result;
}
```

--
#### Await

- lance une task
- bloque en attendant le retour de la tache retour

--

```C#
static void Main(string[] args)
{
  int nb = await MyTask(int value);
}
```
--
#### Attente
--
##### une tache definie
```C#
 task.Wait();
```
--
##### Toute les taches d'une liste 
```C#
List<Task> tasks = new List<Task>();
await Task.WhenAll(tasks);
```
--
##### Une des taches d'une liste 
```C#
List<Task> tasks = new List<Task>();

while (tasks.Count > 0)
{
    Task finished = await Task.WhenAny(tasks);
    //do some stuff
    tasks.Remove(finished);
}
```
--
##### Remarque sur les task.

- Si une task doit etre repeter avec la meme fonction => utilisation de `new Task()`

---

## Sources
[Threading in C#](http://www.albahari.com/threading)